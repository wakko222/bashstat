#!/bin/sh
while :
do
	setpoint=$(cat /opt/bashstat/setpoint)
	echo "Setpoint: "$setpoint
	cel=$(cat /sys/bus/w1/devices/28-0000098b17c0/w1_slave | grep -i t | cut -c30-35)
	temp=$(echo "($cel*9/5+32000)/1000"|bc)
	echo "Current Temp: "$temp
	first=$(echo $temp|cut -c1-2)
	second=$(echo $temp|cut -c3-5)
	if [ "$temp" -lt "$setpoint" ]; then
		echo "1" > /sys/class/gpio/gpio21/value
	else
		echo "0" > /sys/class/gpio/gpio21/value	
	fi
	value=$(cat /sys/class/gpio/gpio21/value)
	echo "Value: "$value
	sleep 300 
done
